require('dotenv').config({
  path: 'config.env'
});

/**
 * Function that forwards the payload to the App Engine endpoint
 *
 * @param {object} payload The payload to forward to app engine
 * @param {object} traceId The traceId for the job
 * @param {object} the fetch object that sends http request to the app engine endpoint
 * @param {function} printLog prints the log to the stackdriver
 */


function msgFwd(payload, traceId, fetch, printLog) {

  obj = JSON.stringify(payload);

  fetch(process.env.ENDPOINT, {
      method: 'POST',
      retries: 3,
      retryDelay: 30000,
      headers: {
        'X-Goog-Resource-Id': traceId,
        'Content-Type': 'application/json'
      },
      body: obj

    })
    .then(function(res) {
      if (res.status === 201) {
        printLog(traceId, 'Message delivered to Kirby', payload.source.bucket, payload.destination.table);
      } else {
        printLog(traceId, 'Issue with the request to Kirby', payload.source.bucket, payload.destination.table);
      }
    });
}

module.exports = msgFwd;
