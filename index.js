const util = require('util');
const payloadGen = require('./payloadGen.js');
const msgFwd = require('./msgFwd.js');
const fetch = require('fetch-retry');
const uuid = require('uuid/v4');
const csvHeader = require('./csvHeaderConfig.json');
const printLog = require('./printLog.js');

/**
 * Background Cloud Function to be triggered by Cloud Storage.
 *
 * @param {object} event The Cloud Functions event trigger.
 * @param {function} callback The callback function.
 */


exports.primeBucketNotification = function(event, callback) {
  const storageMessage = event.data;
  const traceID = uuid();

  //process only newly created objects in the GCS bucket

  if (storageMessage.resourceState === 'exists' && storageMessage.metageneration === '1') {

    printLog(traceID, 'New File landed', storageMessage.bucket, storageMessage.name);

    const payload = payloadGen(traceID, storageMessage.bucket, storageMessage.name, csvHeader, printLog);

    if (payload) {
      printLog(traceID, 'Forwarding the payload to Kirby', storageMessage.bucket, storageMessage.name);
      msgFwd(payload, traceID, fetch, printLog);
    } else {
      printLog(traceID, 'Issue in payload generation', storageMessage.bucket, storageMessage.name);
    }

  }
  callback();
};
