/**
 * Generates the payload based on the cloud bucket notification details
 *
 * @param {object} traceId The traceId for the job
 * @param {object} bucktName The bucketName for the datasource
 * @param {object} object the object in the bucket that triggered the cloudfunction event
 * @param {object} csvHeader the loadConfig details for the BQ table
 */


function payloadGen(traceID, bucketName, object, csvHeader, printLog) {

  const dataset = bucketName.split('_')[1];
  const table = object.split('.')[0];

  var payload = {};
  payload.source = {};
  payload.source.bucket = bucketName;
  payload.source.object = object;
  payload.destination = {};
  payload.destination.dataset = dataset;
  payload.destination.table = csvHeader[table].tableName;
  payload.loadConfig = {};

  if (csvHeader[table].skipLeadingRows) {
    payload.loadConfig.skipLeadingRows = csvHeader[table].skipLeadingRows;
  }

  if (csvHeader[table].fieldDelimiter) {
    payload.loadConfig.fieldDelimiter = csvHeader[table].fieldDelimiter;
  }

  printLog(traceID, 'PayLoad Generated', bucketName, object);
  return payload;

}

module.exports = payloadGen;
